import React, {useState, useEffect, createContext} from 'react';
import * as eva from '@eva-design/eva';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const ThemeContext = createContext({})

export const ThemeProvider = ({ children }) => {
    const [theme, setTheme] = useState('light')

    useEffect(() => {
      AsyncStorage.getItem('theme').then(res => {
        if(res){
          setTheme(res)
        }
        else{
          setTheme('light')
        }
      })
    }, [])
    
    const toggleTheme = async() => {
      const nextTheme = theme === 'light' ? 'dark' : 'light';
      await setTheme(nextTheme);
      await AsyncStorage.setItem('theme', nextTheme)
    };

    return (
      <ThemeContext.Provider value={[theme, toggleTheme]}>
        {children}
      </ThemeContext.Provider>
    )
}