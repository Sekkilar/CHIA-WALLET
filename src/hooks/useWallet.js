import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useState, useEffect, createContext} from 'react';
import {addWallet, getAllWallets, deletWallet} from '../db/Query'

export const WalletContext = createContext({})

export const WalletProvider = ({ children }) => {
    const [wallet, setWallet] = useState()
    const [walletDetails, setWalletDetails] = useState(false)

    useEffect(() => {
      getWallet()
      return () => wallet
    }, [])


    const getWallet = async() => {
        const walletDetails = await getAllWallets();
        if(walletDetails.length){
          await setWallet(true)
          await setWalletDetails(walletDetails)
        }
    }

    const walletSetter = async(data) => {
      try{
        delete data.seed_phrase
      }
      catch(e){
        console.log(e)
      }
      await addWallet(data.XCH_address, data.fingerprintNo);
      await setWallet(true);
      await setWalletDetails(data);
      await getWallet()
    }

    const clearWallet = async(fingerPrintNo) => {
      await deletWallet(fingerPrintNo);
      await getWallet()
    }

    return (
      <WalletContext.Provider value={{wallet, walletDetails, setWallet, setWalletDetails, clearWallet, walletSetter}}>
        {children}
      </WalletContext.Provider>
    )
}