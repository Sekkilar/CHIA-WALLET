import React, {useState, useEffect, useContext} from 'react';
import {
    Card,
    useStyleSheet,
    StyleService,
    Spinner,
    Icon
} from '@ui-kitten/components';
import { SvgCssUri } from 'react-native-svg';
import Clipboard from '@react-native-community/clipboard';
import Snackbar from 'react-native-snackbar';
import { GetBalance, GetBalanceByFingerPrint } from '../../connections/BackendApi'
import {strings} from '../../utils/i18n';

import {
    View,
    TouchableOpacity,
    Text
} from 'react-native';
import { deletWallet } from '../../db/Query';
import { WalletContext } from '../../hooks/useWallet';

const WalletCard = ({
    eva,
    data
}) => {

    const {clearWallet} = useContext(WalletContext)

    const pulseIconRef = React.useRef();
    React.useEffect(() => {
        if(pulseIconRef.current){
            pulseIconRef.current.startAnimation();
        }
    }, []);

    const copyToClipBoard = async() => {
        await Clipboard.setString(data.walletaddress);
        pulseIconRef.current.startAnimation();
        Snackbar.show({
            text: strings("MainAppScreen.clipboard_copy"),
            duration: Snackbar.LENGTH_SHORT,
        });
    }

    const [balance, setBalance] = useState(0.00)
    const [fetching, setFetching] = useState(true);

    useEffect(() => {
        request();
    }, [])

    const request = async() => {
        const resp = await GetBalanceByFingerPrint(data.fingerprintNo)
        if(resp && resp.balance){
            setBalance(resp.balance) 
        }
        else if(resp.errNo === 3){
            await Snackbar.show({
                text: "Wallet not availble, please restore with your seed phrase",
                duration: Snackbar.LENGTH_SHORT,
            });
            await clearWallet(data.fingerprintNo)
        }
        else{
            console.log(resp)
            setBalance(0.00) 
        }
        setFetching(false)
    }

    const style = useStyleSheet(themedStyles)

    return (
        <Card
            onLongPress={() => console.log('menu')}
            style={{ marginVertical: 15 }}
        >
            <View style={style.walletContainer}>
                <View style={style.card}>
                    <SvgCssUri 
                        height={70}
                        width={70}
                        style={style.walletIcon}
                        uri= 'https://www.chia.net/img/chia_logo.svg'
                    />
                    <Text category='h6' >
                        {fetching ? <Spinner size='large' style={{color: 'white'}}/> : `$ ${balance}`}
                    </Text>
                    <Icon 
                        onPress={() => copyToClipBoard()}
                        fill={eva.theme['color-primary-600']} 
                        style={{ height: 30, width: 30}} 
                        name='clipboard' 
                        ref={pulseIconRef}
                        animation='pulse'
                    />
                </View>
            </View>
        </Card>
    )
}

const themedStyles = StyleService.create({
    walletContainer: {
        // marginTop: 40,
        display: 'flex',
        // padding: 10,
        elevation: 2,
    },
    card: {
        display: 'flex',
        flexDirection :'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottom: 2,
        borderColor: 'green',
    }
})

export default WalletCard;