import React, {useContext, useEffect} from 'react';
import  {
    withStyles,
    Layout,
    Text
} from '@ui-kitten/components'
import { WalletContext } from '../hooks/useWallet';

const SplashScreenScreenComponent = ({
    eva,
    navigation
}) => {

    return (
        <Layout style={eva.style.container}>
            <Text category='h1'>
                CHIA WALLET
            </Text>
        </Layout>
    )
}

const SplashScreen = withStyles(SplashScreenScreenComponent, theme => ({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: theme['color-primary-500']
    },
    topBar: {
        backgroundColor: theme['color-primary-100']
    }
}));

export default SplashScreen;

