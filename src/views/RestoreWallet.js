import React, { useContext, useState } from 'react';
import  {
    withStyles,
    Layout,
    Text,
    Icon, 
    TopNavigation,
    Input, Button, Divider, Spinner
} from '@ui-kitten/components';
import {View, TouchableOpacity} from 'react-native';
import DocumentPicker from 'react-native-document-picker';
import {strings} from '../utils/i18n';
import {
    Formik,
} from 'formik';
import * as Yup from 'yup';
import {RestoreWallet} from '../connections/BackendApi'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { WalletContext } from '../hooks/useWallet';

const RestoreValidationSchema = Yup.object().shape({
    seedPhrase: Yup.string().test('test-name', 
        function(value) {
            const { path, createError } = this;
            if(value){
                if(value.split(/ +/).length !== 24){
                    return createError({ path, message: 'The string is not length of 24' });
                }
                else {
                    return true
                }
            }
    })
    .required('Please fill out this field')
})

const RestoreWalletScreenComponent = ({
    eva,
    navigation
}) => {
    const [fileType, setFileType] = React.useState(false);
    

    const {setWallet, setWalletDetails, walletSetter} = useContext(WalletContext)

    const filePick = async() => {
        const res = await DocumentPicker.pick({
            type: [DocumentPicker.types.allFiles],
        });
    }

    const Action = async(values) => {
        const resp = await RestoreWallet(values.seedPhrase)
        if(resp){
            walletSetter(resp);
            return true
        }
        else {
            return null
        }
        
    }

    return (
        <>
            <TopNavigation 
                accessoryLeft={ () =>
                    navigation.canGoBack() ?
                    <Icon
                        style={eva.style.icon}
                        fill={eva.theme['color-primary-300']}
                        name='arrow-back'
                        onPress={() => navigation.goBack()} 
                    />: null
                } 
                style={eva.style.topBar} 
                title={strings('RestoreWallet.title')}
            />
            <Divider />
            <Layout style={eva.style.container}>
                <Formik
                    initialValues={{ seedPhrase: '' }}
                    onSubmit={async(values, actions) =>  {
                        const resp = await Action(values, actions)
                        await actions.setSubmitting(false)
                        if(resp){
                            navigation.navigate("MainAppScreen")
                        }
                    }}
                    validationSchema={RestoreValidationSchema}
                >
                        {({
                            handleSubmit,
                            isSubmitting,
                            values,
                            isValidating,
                            handleChange,
                            errors,
                            touched
                        }) => (
                            <>
                            <View style={eva.style.form}>
                                <Text category='p1'>
                                    {strings('RestoreWallet.message')}
                                </Text>
                                
                                <View style={eva.style.fileInput}>
                                    <Text category='c2'>{strings('RestoreWallet.input_label')}</Text>
                                    <TouchableOpacity onPress={() => setFileType(!fileType)}>
                                        <Text 
                                            category='s1' 
                                            style={{ color: eva.theme['color-primary-500'] }}
                                        >
                                            {fileType ? strings('RestoreWallet.text_type'): strings('RestoreWallet.file_type')}
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                                {fileType ?
                                    <View style={eva.style.filePick}>
                                            <Text 
                                                category='s1' 
                                                style={{ color: eva.theme['color-primary-transparent-600'] }}
                                                onPress={() => filePick()}
                                            >
                                            {strings('RestoreWallet.restore_from')}
                                            </Text>
                                    </View>:
                                    <>
                                    <Input 
                                        disabled={isSubmitting}
                                        multiline
                                        placeholder={strings('RestoreWallet.input_placeholder')}
                                        textStyle={{ minHeight: 64 }}
                                        value={values.seedPhrase}
                                        onChangeText={handleChange('seedPhrase')}
                                        name='seedPhrase'
                                    />
                                    <Text category='label' status={errors.seedPhrase && touched.seedPhrase ? 'danger': 'info'}>
                                        {errors.seedPhrase && touched.seedPhrase ? errors.seedPhrase: "The seed phrase whhen you created wallet" }
                                    </Text>
                                    </>
                                }
                            </View>
                            <View style={{paddingHorizontal: 30, paddingBottom: 10}}>
                                <Button
                                    disabled={!values.seedPhrase || isSubmitting}
                                    onPress={handleSubmit}
                                    accessoryLeft={() => isSubmitting ? <Spinner size='small' style={{color: 'white'}}/>: null}
                                >
                                    {strings('RestoreWallet.button')}
                                </Button>
                                {isSubmitting && <Text status='warning' category='c1' style={{ textAlign: 'center' }}>
                                    {strings('CreateNewWallet.load_message')}
                                </Text>}
                            </View>
                            </>
                        )}
                </Formik>
                
            </Layout>
        </>
    )
}

const RestoreWalletScreen = withStyles(RestoreWalletScreenComponent, theme => ({
    container: {
      flex: 1,
    },
    topBar: {
        height: 50,
        display: 'flex', justifyContent: 'center'
    },
    form: {
        flex:1,
        top: 20,
        paddingHorizontal: 30
    },
    icon: {
        width: 32,
        height: 32,
        marginRight: 10
    },
    fileInput: {
        marginTop: 20, display: 'flex',
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginBottom: 15
    },
    filePick: {
        height: 100,
        backgroundColor: theme['color-info-100'],
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
}));

export default RestoreWalletScreen;

