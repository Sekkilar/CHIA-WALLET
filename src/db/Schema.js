export const WALLETS = "WALLETS";
export const WALLETS_SCHEMA = {
  name: WALLETS,
  properties: {
    walletaddress: {type: 'string'},
    fingerprintNo: {type: 'string', indexed: true}
  },
  primaryKey: "fingerprintNo",
}