import Realm from './index';
import {WALLETS} from './Schema';


export const addWallet = (walletaddress, fingerprintNo) => {
    Realm.write(async() => {
        const resp = Realm.create(WALLETS, { walletaddress: walletaddress, fingerprintNo: fingerprintNo });
        console.log(typeof resp, resp)
        return resp
    });
}

export const getAllWallets = () => {
    const resp = Realm.objects(WALLETS)
    return resp
}

export const deletWallet = fingerprintNo => {
    Realm.write(async() => {
        const resp = Realm.objectForPrimaryKey(WALLETS, fingerprintNo);
        console.log(typeof resp, resp)
        Realm.delete(resp)
    });
}