import React, { useContext, useEffect, useState } from "react";
import { createStackNavigator } from "@react-navigation/stack";
import FromStartScreen from '../views/FromStartScreen';
import NewWalletScreen from '../views/CreatenewWallet';
import RestoreWalletScreen from '../views/RestoreWallet';
import MainApp from './BottomNavigator';
import CopySeedPhraseScreen from '../views/CopySeedPhrase';
import {WalletContext} from '../hooks/useWallet'

const Stack = createStackNavigator();

const MainStack = () => {
  
   return (
    <Stack.Navigator
      // initialRouteName={initial}
      screenOptions={{ headerShown: false }}
    >
      <Stack.Screen name="FromStartScreen" component={FromStartScreen} />
      <Stack.Screen name="MainAppScreen" component={MainApp} />
      <Stack.Screen name="NewWalletScreen" component={NewWalletScreen} />
      <Stack.Screen name="RestoreWalletScreen" component={RestoreWalletScreen} />
      <Stack.Screen name="CopySeedPhraseScreen" component={CopySeedPhraseScreen} />
    </Stack.Navigator>
  );
};

export default MainStack;