import React, {useContext, useEffect} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import * as eva from '@eva-design/eva';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import { NavigationContainer } from '@react-navigation/native'; 
import { SafeAreaView } from 'react-native';

import MainAppStack from './src/navigation/MainApp';
import { EvaIconsPack } from '@ui-kitten/eva-icons';

import {ThemeProvider, ThemeContext} from './src/hooks/useTheme';
import { WalletContext, WalletProvider } from './src/hooks/useWallet';
import  {appTheme} from './src/config/appTheme';

export default function App() {
  return (
    <WalletProvider>
      <ThemeProvider>
        <AppProviders />
      </ThemeProvider>
    </WalletProvider>
  );
}

const AppProviders = () => {
  const [theme] = useContext(ThemeContext);
  const {wallet} = useContext(WalletContext)
  useEffect(() => {

  }, [wallet])
  
  return <SafeAreaView style={{flex:1}}>
        <NavigationContainer>
          <IconRegistry icons={EvaIconsPack} />
          <ApplicationProvider {...eva} theme={{ ...eva[theme], ...appTheme }}>
            <MainAppStack />
          </ApplicationProvider>
        </NavigationContainer>
    </SafeAreaView>
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
